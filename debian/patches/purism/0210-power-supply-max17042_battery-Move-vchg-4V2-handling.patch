From: Sebastian Krzyszkowiak <sebastian.krzyszkowiak@puri.sm>
Date: Sat, 7 Nov 2020 05:55:52 +0100
Subject: power: supply: max17042_battery: Move vchg-4V2 handling to POR
 initialization

It has to be done just once as part of POR init. Doing it at each boot
resets the gauge state, so it can't learn the battery characteristics
over time.
---
 drivers/power/supply/max17042_battery.c | 28 ++++++++++++++++------------
 include/linux/power/max17042_battery.h  |  2 +-
 2 files changed, 17 insertions(+), 13 deletions(-)

diff --git a/drivers/power/supply/max17042_battery.c b/drivers/power/supply/max17042_battery.c
index 9c17560..07c2cb0 100644
--- a/drivers/power/supply/max17042_battery.c
+++ b/drivers/power/supply/max17042_battery.c
@@ -814,6 +814,12 @@ static inline void max17042_override_por_values(struct max17042_chip *chip)
 	    (chip->chip_type == MAXIM_DEVICE_TYPE_MAX17055)) {
 		max17042_override_por(map, MAX17047_V_empty, config->vempty);
 	}
+
+	if (chip->chip_type &&
+	    (chip->chip_type == MAXIM_DEVICE_TYPE_MAX17055)) {
+		max17042_override_por(map, MAX17055_ModelCfg, config->model_cfg);
+	}
+
 }
 
 static int max17042_init_chip(struct max17042_chip *chip)
@@ -928,12 +934,19 @@ max17042_get_of_pdata(struct max17042_chip *chip)
 	struct device *dev = &chip->client->dev;
 	struct device_node *np = dev->of_node;
 	u32 prop;
+	u64 data64;
 	struct max17042_platform_data *pdata;
 
 	pdata = devm_kzalloc(dev, sizeof(*pdata), GFP_KERNEL);
 	if (!pdata)
 		return NULL;
 
+	pdata->config_data = devm_kzalloc(dev, sizeof(*pdata->config_data), GFP_KERNEL);
+	if (!pdata->config_data)
+		return NULL;
+
+	pdata->config_data->model_cfg = MAX17055_REFRESH_BIT;
+
 	/*
 	 * Require current sense resistor value to be specified for
 	 * current-sense functionality to be enabled at all.
@@ -951,8 +964,9 @@ max17042_get_of_pdata(struct max17042_chip *chip)
 		pdata->vmin = INT_MIN;
 	if (of_property_read_s32(np, "maxim,over-volt", &pdata->vmax))
 		pdata->vmax = INT_MAX;
-	if (of_property_read_bool(np, "maxim,vchg-4V2"))
-		pdata->vchg=MAX17055_VCHG_4V2;
+	if (!of_property_read_bool(np, "maxim,vchg-4V2"))
+		pdata->config_data->model_cfg |= MAX17055_VCHG_BIT;
+
 
 	return pdata;
 }
@@ -1003,7 +1017,6 @@ max17042_get_default_pdata(struct max17042_chip *chip)
 	pdata->vmax = MAX17042_DEFAULT_VMAX;
 	pdata->temp_min = MAX17042_DEFAULT_TEMP_MIN;
 	pdata->temp_max = MAX17042_DEFAULT_TEMP_MAX;
-	pdata->vchg = MAX17055_VCHG_4V3;
 
 	return pdata;
 }
@@ -1069,7 +1082,6 @@ static int max17042_probe(struct i2c_client *client,
 	int ret;
 	int i;
 	u32 val;
-	u16 mask, bits;
 
 	if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_WORD_DATA))
 		return -EIO;
@@ -1124,14 +1136,6 @@ static int max17042_probe(struct i2c_client *client,
 		regmap_write(chip->regmap, MAX17042_LearnCFG, 0x0007);
 	}
 
-	if (chip->chip_type &&
-	    (chip->chip_type == MAXIM_DEVICE_TYPE_MAX17055)) {
-		mask = MAX17055_VCHG_BIT | MAX17055_REFRESH_BIT;
-		bits = MAX17055_REFRESH_BIT | (chip->pdata->vchg ?
-					       MAX17055_VCHG_BIT : 0 );
-		regmap_write_bits(chip->regmap, MAX17055_ModelCfg, mask, bits);
-	}
-
 	chip->battery = devm_power_supply_register(&client->dev, max17042_desc,
 						   &psy_cfg);
 	if (IS_ERR(chip->battery)) {
diff --git a/include/linux/power/max17042_battery.h b/include/linux/power/max17042_battery.h
index eaff17a..1fb98e8 100644
--- a/include/linux/power/max17042_battery.h
+++ b/include/linux/power/max17042_battery.h
@@ -213,6 +213,7 @@ struct max17042_config_data {
 	u16	full_soc_thresh;	/* 0x13 */
 	u16	design_cap;	/* 0x18 */
 	u16	ichgt_term;	/* 0x1E */
+	u16	model_cfg;	/* 0xDB */
 
 	/* MG3 config */
 	u16	at_rate;	/* 0x04 */
@@ -266,7 +267,6 @@ struct max17042_platform_data {
 	int         vmax;	/* in millivolts */
 	int         temp_min;	/* in tenths of degree Celsius */
 	int         temp_max;	/* in tenths of degree Celsius */
-	int         vchg;       /* charge voltage, 1 = >4.25V, 0 = 4.2V */
 };
 
 #endif /* __MAX17042_BATTERY_H_ */
